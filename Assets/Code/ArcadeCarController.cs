using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ArcadeRigidbodyCarController : MonoBehaviour
{
    public float forceSpeed = 10f;
    public float torqueSpeed = 100f;

    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        rb.AddForce(forceSpeed * verticalInput * transform.forward * Time.fixedDeltaTime);
        rb.AddRelativeTorque(Vector3.up * torqueSpeed * horizontalInput * Time.fixedDeltaTime);
    }
}